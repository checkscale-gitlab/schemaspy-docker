FROM openjdk:slim-buster

# Update Packages
RUN apt-get update && apt-get -q upgrade -y && apt-get -qq install jq curl unzip graphviz -y

# AWS
ADD https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip awscliv2.zip 
RUN unzip -q awscliv2.zip && ./aws/install

ADD https://github.com/schemaspy/schemaspy/releases/download/v6.1.0/schemaspy-6.1.0.jar schemaspy.jar
ADD https://jdbc.postgresql.org/download/postgresql-9.4.1212.jar postgresql.jar
ADD https://downloads.mariadb.com/Connectors/java/connector-java-2.7.1/mariadb-java-client-2.7.1.jar mariadb.jar

# Test
RUN aws --version && java --version
# RUN java -jar schemaspy.jar --help
